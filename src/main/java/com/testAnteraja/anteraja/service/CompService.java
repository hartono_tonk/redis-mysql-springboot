package com.testAnteraja.anteraja.service;

import com.testAnteraja.anteraja.model.DataModel;

public interface CompService {
    public DataModel save(DataModel dataModel);

    public DataModel update(DataModel dataModel);

    public DataModel get(int id);

    public void delete(DataModel student);
}
