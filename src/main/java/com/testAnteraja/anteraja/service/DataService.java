package com.testAnteraja.anteraja.service;

import com.testAnteraja.anteraja.model.DataModel;
import com.testAnteraja.anteraja.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("dataService")
public class DataService {
    @Autowired(required = false)
    private DataRepository dataRepository;


    @Transactional
    @CachePut(value = "product", key = "#dt.id")
    public DataModel creat(DataModel dt){
        return dataRepository.save(dt);
    }

    @Cacheable("#allProduct")
    public List<DataModel> getData(){
        return (List<DataModel>) dataRepository.findAll();
    }


    public List<DataModel> findByPrice(Double price){
        return dataRepository.findByPrice(price);
    }

}
