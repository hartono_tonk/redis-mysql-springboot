package com.testAnteraja.anteraja;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
public class AnterajaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnterajaApplication.class, args);
	}
}
