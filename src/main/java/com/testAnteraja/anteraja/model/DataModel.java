package com.testAnteraja.anteraja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "master_price")
public class DataModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "origin_code", nullable = false)
    private String origin_code;
    @Column(name = "destination_code", nullable=false)
    private String destination_code;
    @Column(name = "price", nullable = false)
    private double price;
    @Column(name = "product", nullable = false)
    private String product;
}