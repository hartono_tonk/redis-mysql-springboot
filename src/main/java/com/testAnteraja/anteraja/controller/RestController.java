package com.testAnteraja.anteraja.controller;

import com.testAnteraja.anteraja.model.DataModel;
import com.testAnteraja.anteraja.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {

    @Autowired
    DataService dataService;



    @PostMapping(value = "/product")
    public DataModel creat(@RequestBody DataModel dt){
        return dataService.creat(dt);
    }

    @GetMapping(value = "/product")
    public List<DataModel> tarikData(){
        return dataService.getData();
    }

    @GetMapping(value = "/product/{price}")
    public List<DataModel> findPrice(DataModel price){
        return dataService.findByPrice(price.getPrice());
    }


}
