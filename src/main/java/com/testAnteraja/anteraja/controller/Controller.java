package com.testAnteraja.anteraja.controller;


import com.testAnteraja.anteraja.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    DataService dataService;


    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    @RequestMapping("/insert")
    public String insert() {
        return "insert";
    }




}
