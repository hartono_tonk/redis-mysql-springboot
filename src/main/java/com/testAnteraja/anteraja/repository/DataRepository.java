package com.testAnteraja.anteraja.repository;


import com.testAnteraja.anteraja.model.DataModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface DataRepository extends CrudRepository<DataModel, Serializable> {

    public List<DataModel> findByPrice(Double price);
}
