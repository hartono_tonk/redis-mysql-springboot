package com.testAnteraja.anteraja.jpaConfig;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.testAnteraja.anteraja.repository")
@EnableCaching
public class JpaConfig {

}