$(document).ready(function () {
  $("form").submit(function (event) {
    event.preventDefault();
    ajaxPost();
    });

function ajaxPost(){
    var formData = {
      origin_code: $("#origin").val(),
      destination_code: $("#destination").val(),
      price: $("#price").val(),
      product: $("#product").val()
    }
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: "/api/product",
      data: JSON.stringify(formData),
      dataType: "json",
      success : function(result){
                console.log(result);
                window.location.replace("/index");
                }
      });
    }
});
