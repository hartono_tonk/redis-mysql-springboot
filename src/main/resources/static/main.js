$(document).ready (function() {
    var table = $('#tabel').DataTable({
        "sAjaxSource": "/api/product",
        "sAjaxDataProp": "",
        "order": [[ 0, "asc" ]],
        "columns": [
            { data: "origin_code", name: "origin_code" },
            { data: "destination_code", name: "destination_code" },
            { data: "price", name:"price" },
            { data: "product", name:"product" }
        ]
    });
});

